import pandas as pd
#import os
from matplotlib import pyplot as plt

max_time = 400e3

results = pd.read_csv('../experiments/all_performance.csv')

results_NFSP = results[['timestep1', 'NFSP']].dropna()
results_NFSP = results_NFSP[results_NFSP['timestep1']< max_time]

results_DQN = results[['timestep2', 'DQN']].dropna()
results_DQN = results_DQN[results_DQN['timestep2']<max_time]

results_PPO = results[['timestep3', 'PPO - 1stepAdv']].dropna().iloc[::10,:]
results_PPO = results_PPO[results_PPO['timestep3']<max_time]

results_PPO_2 = pd.read_csv("../experiments/nolimit_holdem_ppo_result_adv_100/performance.csv")
results_PPO_2 = results_PPO_2.loc[results_PPO_2.timestep != "timestep"]
results_PPO_2['timestep'] = results_PPO_2['timestep'].astype(int)
results_PPO_2 = results_PPO_2[results_PPO_2['timestep'] < max_time]
results_PPO_2 = results_PPO_2.sort_values('timestep', ascending=True)


smoothing = 10
line1,  = plt.plot(results_DQN['timestep2']/1000, results_DQN['DQN'].rolling(10).mean())
line2,  = plt.plot(results_NFSP['timestep1']/1000, results_NFSP['NFSP'].rolling(10).mean())
line3, = plt.plot(results_PPO['timestep3']/1000, results_PPO['PPO - 1stepAdv'].rolling(10).mean())
line4, = plt.plot(results_PPO_2['timestep']/1000, results_PPO_2['reward'].rolling(30).mean(), color='pink')
plt.xlabel('Timestep(1e3)')
plt.ylabel('reward')
plt.title('Learning Curves')
plt.legend((line1, line2, line3, line4), ('DQN', 'NFSP', 'PPO - 1stepAdv', 'PPO - fullAdv'))
#plt.show()

plt.savefig('learning_curves.png')