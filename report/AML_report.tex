\documentclass{article}
%\documentclass[sigconf]{acmart}

% if you need to pass options to natbib, use, e.g.:
%     \PassOptionsToPackage{numbers, compress}{natbib}
% before loading neurips_2020

% ready for submission
% \usepackage{neurips_2020}

% to compile a preprint version, e.g., for submission to arXiv, add add the
% [preprint] option:
    \usepackage[preprint]{neurips_2020}

% to compile a camera-ready version, add the [final] option, e.g.:
%     \usepackage[final]{neurips_2020}

% to avoid loading the natbib package, add option nonatbib:
    %  \usepackage[nonatbib]{neurips_2020}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography
\usepackage{graphicx}

\title{Deep Reinforcement Learning in No-Limit Hold'em}

% \setlength\parindent{24pt}

%\AtBeginDocument{%
%  \providecommand\BibTeX{{%
%    \normalfont B\kern-0.5em{\scshape i\kern-0.25em b}\kern-0.8em\TeX}}}

% The \author macro works with any number of authors. There are two commands
% used to separate the names and addresses of multiple authors: \And and \AND.
%
% Using \And between authors leaves it to LaTeX to determine where to break the
% lines. Using \AND forces a line break at that point. So, if LaTeX puts 3 of 4
% authors names on the first line, and the last on the second line, try using
% \AND instead of \And before the third author name.


\author{
    Mark Aksen \\
    Columbia University \\ 
    \texttt{mja2213@columbia.edu}
    \And
    Rahul Bhatia \\ 
    Columbia University \\ 
    \texttt{rb3318@columbia.edu)}
    \AND % capital AND forces line break
    Steven Friedman \\
    Columbia University \\ 
    \texttt{smf2221@columbia.edu}
    \And
    Nikoletta Giannopoulou \\
    Columbia University \\ 
    \texttt{ng2752@columbia.edu}
}

\begin{document}
\maketitle

\section{Introduction}

    In recent years, there has been great progress in reinforcement learning using game settings as challenging problems and benchmarks. Poker is one such classic game offering numerous challenges in the area of imperfect information game playing. In imperfect information games, players do not have full knowledge of past events (e.g. cards dealt to other players in poker). Classically, Nash equilibrium is a solution describing optimal play in such adversarial games. In fact, in a two-player zero-sum game, a Nash equilibrium strategy is unbeatable and therefore determining one is 'solving' such a game [5]. However, computing the equilibrium solution is far more challenging in imperfect information games with efficient algorithmic approaches (e.g. with polynomial runtime and space). 
    
    Traditional reinforcement learning algorithms, such as Q-learning, have had great success with controlled single agent environments, where continuous exploration is possible. However, these methods often rely on stationary environments, where background factors do not change between episodes. These methods are also deterministic, producing an 'optimal' action for every state. Yet in poker and other imperfect information game settings, deterministic policies will perform poorly, as they will be unable to respond to bluffs. Furthermore, in the case of a player who bluffs every time, an ideal agent would recognize this and update his policy accordingly. 
    
    Determining an optimal stochastic policy naturally leads to policy gradient methods. One such method that has had significant success in dynamic environments is a strategy known as Proximal Policy Optimization (PPO) [2]. This is an actor-critic method with a loss function that clips the probability ratio from previous iterations of the model and allows for incremental changes to the network. Such methods have been applied to various multiplayer Atari games with significant success. 
    
    In this project, our team aims to implement an agent in the RLCard reinforcement learning environment using a PPO method. We will evaluate the strengths and weaknesses of this approach in the environment of No-Limit Texas Hold'em Poker. We have chosen to use the RLCard python package, which provides a reinforcement learning environment for a handful of card games, and is produced by the DATA Lab at Texas A\&M University and community contributors. This set of methods will be compared against a handful of existing techniques, specifically Deep Q Networks and Neural Ficticious Self-Play (NFSP), by evaluating performance against random agents, as well as each other in one on one scenarios. We anticipate the challenge of this project to derive from the partial observability of Hold'em Poker, and we look to see how various different techniques behave under incomplete information.

    A link to our git repository can be found at the end of this document.

\subsection{Relevant Publications}

The most common approach for solving large imperfect information games is Counterfactual regret minimization (CFR). CFR is an iterative algorithm that will converge to Nash equilibrium in two-player zero sum games. In fact recent CFR-based methods have been developed that effectively solve limit Texas Hold'em in a computationally efficient manner [6]. 

No-limit poker has proven to be a more significant challenge, with a significantly larger action space. To reduce the complexity of the game, similar actions are grouped together, reducing the actions space, known in the literature as action abstraction [4]. Due to the success of CFR-based methods for limit poker, they have also been adapted for no-limit Texas Hold'em with the latest state of the art algirhtm known as Pluribus. Pluribus relies on depth-limited Monte Carlo CFR, in which actions are sampled in game tree rather than sampling complete game trees. Pluribus’ success shows that despite the lack of known strong theoretical guarantees on performance in multiplayer games, there are large-scale, complex multiplayer imperfect- information settings in which a carefully constructed self-play-with-search algorithm can produce superhuman strategies. This made us dive deeper into imperfect information games, leading us to read papers such as Safe and Nested Endgame Solving for Imperfect-Information Games [3] and Deep Reinforcement Learning from Self-Play in Imperfect-Information Games [5].

The first speaks about how, in imperfect-information games, the optimal strategy in a subgame may depend on the strategy in other, unreached subgames. Thus a subgame must consider the strategy for the entire game as a whole, which is possible by using subgame solving. Subgame-solving techniques can outperform prior methods both in theory and practice while they can respond to opponent actions that are outside the original action abstraction; this significantly outperforms the prior state-of-the-art approach, action translation.  

The latter paper speaks about the first scalable end-to-end approach to learning approximate Nash equilibria without prior domain knowledge. The method presented, NFSP, combines fictitious self-play with deep reinforcement learning, and in Limit Texas Hold’em, NFSP learned a strategy that approached the performance of state-of-the-art, superhuman algorithms based on significant domain expertise. 

Esssentially, a NFSP agent, by interacting with the other players in a game, memorizes its experience of game transitions and its own behaviour. NFSP treats these memories as two datasets suitable for deep reinforcement learning and supervised classification. It trains a neural network to predict action values from the data using off-policy reinforcement learning, and it also trains another separate neural network to imitate its own past behaviour using supervised classification on the data [5]. Thus, during a play, the agent chooses its actions from a mixture of its two strategies. DQN agents, on the other hand, combine Q-Learning with deep neural using a model-free off-policy reinforcement learning method and trains a critic to estimate the return or future rewards [5].

Conducting extensive literature review has played a crucial role in narrowing down the scope of this research. Considering CFR is pivotal to imperfect information games and has already been researched vastly, we have decided to not further explore it but rather focus on implementing a new agent and compare its perfomance to NFSP and DQN for a change. This brings us to the last training algorithm we have explored through the paper Proximal Policy Optimization Algorithms [2], which proposes a new family of policy gradient methods for reinforcement learning.

\subsection{RL Environment Structure}

In the RLCard No-Limit Texas Hold'em environment, the state contains information about cards and chip counts. The first 52 indices represent which cards agents see including community cards and their own private hand, and subsequent indices represent chip counts of each player. Instead of a continuous action space for raises, the environment supports six actions: Fold, Check, Call, Raise Half Pot, Raise Full Pot, and All In. Rewards are given at the end of a hand and are based on big blinds per hand, so if an agent wins or loses double the big blind, the reward will be +2 or -2, respectively. For performance metrics, we evaluate each of our agents against a random agent, both in training and evaluation sessions. In addition, we determine the performance of our PPO agent against the 

\section{Methods}

\subsection{First Steps}

We started with a Deep-Q Network as this approach was already implemented in the RLCard environment. Following the RLCard paper, we lightly tuned hyperparameters. The memory size is selected in \{2000, 100000\} and the discount factor is 0.99. The network is optimized using an Adam optimizer with learning rate 0.00005, and network structure is a multi-layer perceptron (MLP) with size 10-10, 128-128, or 515-512. We train our network every iteration and evaluate every 1000 hands of training.  

In addition, we train an agent based on Neural fictitious self play (NFSP) method; this algorithm has also been implemented in the RLCard environment. 

\subsection{Building an Agent}

Our RL Agent implements a Proximal Policy Optimization learning method. We chose this method because we believe it is more sample efficient approach than other deep reinforcement learning algorithms for our task. Our actor network structure is an MLP with layer sizes 64-64. Our hypothesis is that this approach will yield better performance in the regime of limited training data.

Our generalized advantange estimator has a discount factor of $\gamma = 0.9$ with a 1 step look-ahead. 

\section{Results}


\begin{figure}[h]
Beyond implementation of the Proximal Policy Optimization agent in RLCard, we also wanted to capture it's efficiency at learning how to play No-Limit Hold'em when compared to the existing DQN and NFSP agents provided. For this purpose, we have trained the PPO Agent under the same game setup: heads-up against a single, random agent. Our hypothesis was that the PPO style learning would learn more quickly how to take advantage of an agent that plays randomly and thus achieve a higher expected reward earlier in the training process.
\centering
\includegraphics[width=10cm]{learning_curves}
\caption{Learning curves in terms of performance against random agents. X-axis represents total steps taken in the environment, measured in terms of number of small blinds. Y-axis is the reward achieved by playing against random agents. }
\end{figure}

In Figure 1, we see the learning curves for our various approaches. DQN and NFSP represent the learning rates of the out of the box DQN and NFSP agents provided as samples in the RLCard project. Note: The PPO 1stepAdv learning curve indicates the agent we wrote trained using a 1 step look ahead advantage, as opposed to the generalized advantage estimator. The PPO - fullAdv curve represents our final PPO implemntation with the generalized advantage estimator. Notably, due to performance issues and time constraints, we were not able to finish training this to the full length as the others. In addition, we have smoothed this curve using a linear interplation for visual clarity.

We observe a few notable behaviors here. \  
Firstly, the DQN agent seems to strongly outperform other strategies. \ 
This may be the case, because DQN methods generally perform well for static environments, such as against a random Poker agent. \ 
More importantly, however, we wanted to compare the performance of the PPO Agent. \ 
We present two type here: the PPO Agent with a one step lookahead ($PPO_1$) for reward and a PPO agent using the generalized advantage estimator($PPO_{GAE}$). \ 
We find that $PPO_1$ performs poorly for the first several thousand timesteps, and undergoes a large jump as it spends more iterations. \ 
While it's not necessarily the case, this seems to be the result of the time it takes the critic to learn advantageous states when it's only provided a 1 step forward reward value. \ 
In contrast, the $PPO_{GAE}$ agent seems to learn in a more quasi-monotonic fashion, as it's critic receives a discounted future value of rewards associated as an advantage estimate. 

All things considered, we did not find that the PPO agent under our hyperparameterization would learn to beat a random agent faster than the DQN agent. While this could have been for many reasons, we believe that further work in identifying better hyperparameters would yield more insight as to why this behvaior is seen.

\section{Discussion}


In this project, we apply Deep Q-Network (DQN), Neural Fictitious Self-Play (NFSP), and Proximal Policy Optimization (PPO) to the No-Limit Texas Hold'em Poker environment provided by RLCard in order to test the performance of our PPO algorithm. The algorithms belong to different categories: DQN is a standard single-agent reinforcement learning algorithm, NFSP is a deep reinforcement learning approach for extensive games with imperfect information, and PPO is a policy gradient method. Our approach is to evaluate the performance of the agents based on wining rates, with good agents being rewarded, and poor agents being discarded. While DQN is the best performer, we find that PPO with GAE has continuous performance improvements. Our results should indicate the strengths of the PPO method of training agents when contrasted with the existing methods listed above. Further, we believe that our implementation may lead to the development of an official PPO Agent implementation that our group can contribute to the RLCard public repository.

Some additional experiments that we would like to include is evaluating the agents against each other in a tournament style. Further, it would be interesting to compare against agents with dynamic strategies.

Our source code can be found here \url{https://gitlab.com/rahulbhatiak/holdem-agent}

\section{Bibliography}

[1] Zha, Daochen \ \& Lai, Kwei Herng  \ \& Cao, Yuanpu \ \& Huang, Songyi \ \& Wei, Ruzhe \ \& Guo,Junyu \ \& Hu, Xia (2019). RLCard: {A} Toolkit for Reinforcement Learning in Card Games. \url{http://arxiv.org/abs/1910.04376}.

[2] Schulman, John \ \& Wolski, Filip \ \& Dhariwal, \ \& Radford, Prafulla Alec \ \& Oleg Klimov (2017). Proximal Policy Optimization Algorithms. {\it CoRR}. \url{http://arxiv.org/abs/1707.06347}.

[3] Brown, Noam \ \& Sanholm, Tuomas (2017). Safe and Nested Subgame Solving for Imperfect-Information Games. {\it NeurIPS 2017}. \url{https://papers.nips.cc/paper/2017/file/7fe1f8abaad094e0b5cb1b01d712f708-Paper.pdf}.

[4] Brown, Noam \ \& Sandholm, Tuomas (2019). Superhuman AI for multiplayer poker. {\it Nature}. \url{https://cs.cmu.edu/~noamb/papers/19-Science-Superhuman.pdf}.

[5] Heinrich, Johannes \ \& Silver, David (2016). Deep Reinforcement Learning from Self-Play in Imperfect-Information Games. \url{https://arxiv.org/abs/1603.01121}.

[6] Bowling, Michael \ \& Burch, Neil \ \& Johanson, Michael \ \& Tammelin, Oskari (2015). Heads-up limit hold'em poker is solved.  {\it{Science}} \url{https://science.sciencemag.org/content/347/6218/145}

\end{document}
