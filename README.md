# holdem-agent

This project contains the final project work for Mark Aksen, Nicole Giannopoulou, Rahul Bhatia, and Steven Friedman in the Fall 2020 session of Advanced Machine Learning at Columbia University.

## Overview of Directories

- experiments
  - Contains the output of the training experiments for each model type
- models
  - Contains trained model results for each type of model for comparison scripts
- src
  - Contains Python environment defined by a Pipfile for running this training and evaluation
  - /agents/ppo_agent.py: new PPO agent implementation
  - nolimit_holdem_ppo.py: Python Source Code for training and evaluating the new PPO Model
  - /samples: RLCard No Limit Hold'em training and evaluation examples for existing agents

## Set Up and Running

Note that RLCard requires tensorflow v1.

For this project, we used pipenv, a python project dependency manager to handle our dependency management. If you have this installed, you can replicate our exact python environment using the bellow steps.

Set up the environment with:
```
cd source
pipenv install
```

Train and evaluate PPO:
```
pipenv run train-ppo
```

Train and evaluate DQN or NFSP
```
pipenv run train-dqn
pipenv run train-nfsp
```

Produce the learning_curves image
```
pipenv run evaluate-performance
```

## References

The PPO surrogate objective function is described in this paper by OpenAI researchers:
- Schulman, et al. Proximal Policy Optimization Algorithms (2017). https://arxiv.org/pdf/1707.06347.pdf.

Documentation for the environment including its No Limit Hold'em game is contained at the following site:
- RLCard documentation on environment, game, etc.: http://rlcard.org/

In implementation, the following resources were consulted:
- OpenAI. PPO2 implementation in Baselines codebase. https://github.com/openai/baselines/blob/master/baselines/ppo2/.
- RLCard. Agents and examples in RLCard codebase. https://github.com/datamllab/rlcard.
- Tovar, Alvaro Duran. Advantage Actor Critic (A2C) implementation. https://medium.com/deeplearningmadeeasy/advantage-actor-critic-a2c-implementation-944e98616b
